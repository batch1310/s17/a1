let studentArray = [];

function addStudent(name) {
	studentArray.push(name);
	console.log(`${name} was added to the student's list`);
}

function countStudents() {
	console.log(`There are a total of ${studentArray.length} students enrolled`);
}

function printStudents() {
	studentArray.sort();

	studentArray.forEach(
		function(e) {
			console.log(e);
		}
	)
}

function findStudent(e) {
	if(studentArray.includes(e)){
		return `${e} is an Enrollee.`
	}
	else if(){
		return `${studentArray} are enrollees.`
	}
	else {
		return `No student found with the name ${e}`
	}
}